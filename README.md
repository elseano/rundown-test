# rundown-test

Example showing rundown rendering in GitLab

## Section <r section="first"/>

This is a <r sub-env>$VARIABLE</r>.

<r stdout spinner="Running thing..."/>

``` ruby
puts "Ohai"
```
